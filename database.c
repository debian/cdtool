/* database.c 
 *
 * Copyright 1994 thomas insel
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 2 dated June, 1991.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include <stdio.h>
#include <string.h>
#include <pwd.h>
#include <stdlib.h>
#include <unistd.h>

#include "config.h"
#include "database.h"
#include "util.h"

/* chomp(): conceptually similar to the Perl function of that name.
 * Removes any number of CR or NL chars from the end of a string.
 */
static char *chomp(char *c)
{
    int l=strlen(c)-1;
    while (l>=0 && (c[l]=='\n' || c[l]=='\r'))
	c[l--]='\0';
    return c;
}

/************************************************************************/
/* Procedure:  read_db
 * Purpose:    to read database info for this CD-ROM
 * 
 * Inputs:     tracks info, database
 * Outputs:    to cd_t
 * Returns:    pointer to cd_t structure
 * Notes:  
 *   1.
 */
/************************************************************************/

cd_t *
read_db(char *tracks_buffer, char usedb) {

   /* given "tracks" line in tracks_buffer, search databases for a
    * corresponding entry, return cdname/artist/tracks information
    *
    * if the disc is listed more than once, this routine will return
    * the last entry for the disc from the first file that has any
    * entry for it.
    */

    char *path_env = getenv("CDTOOLDBPATH");
    char *path_bit;
    int  dbpath_count=1, cur_file=0, found_it=0;
    struct passwd *prec = getpwuid(getuid());
    cd_t *temp_cd = (cd_t *)calloc(1,sizeof(cd_t));
    char dbpath[10][255];

    if (!usedb) {
        /* free ((void *)temp_cd);  * can't free this, we're returning it */
        return temp_cd;
    }

    /* first, put default file in search path */
    strcpy(dbpath[0], prec->pw_dir);
    strcat(dbpath[0], "/.cdtooldb");

    /* read search path from environment */
    if (path_env != NULL)
        while (dbpath_count < 9 &&
                    (path_bit = strsep(&path_env, ":\0")) != NULL)
            strcpy(dbpath[dbpath_count++], path_bit);

    /* check files in search path until the disc's entry is found */
    while (!found_it && cur_file < dbpath_count) {
        char read_buffer[2048];
	int  cur_track=0, printing=0;
        FILE *fred = fopen(dbpath[cur_file], "r");
        cur_file++;

        while (fred != NULL && !feof(fred) ) {
            fgets(read_buffer, 2048, fred);
            if (strncmp(read_buffer, tracks_buffer, 
			strlen(tracks_buffer)) == 0) {
                printing = 1;
                found_it = 1;
            } else if (strncmp(read_buffer, "tracks ", 7) == 0) {
                printing = 0;
            } else if (printing) {
                if (strncmp(read_buffer,"track ",6) == 0)
                    strncpy(temp_cd->track_names[cur_track++], 
                                                  &read_buffer[6],99);
                else if (strncmp(read_buffer,"cdname ",7) == 0)
                    strncpy(temp_cd->cdname, &read_buffer[7],99);
                else if (strncmp(read_buffer,"artist ",7) == 0)
                    strncpy(temp_cd->artist, &read_buffer[7],99);
            }
        }

	if (fred) {
	    fclose(fred);
	    chomp(temp_cd->cdname);
	    chomp(temp_cd->artist);
	    while(cur_track--)
		chomp(temp_cd->track_names[cur_track]);
	}
    	else
    	{
		int i;

		temp_cd->cdname[0] = '\0';
		temp_cd->artist[0] = '\0';
		for (i=0 ; i<100 ; i++ )
			temp_cd->track_names[i][0] = '\0';
	}
    } /*while*/
    return temp_cd;
}
