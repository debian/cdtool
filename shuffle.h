#ifndef __SHUFFLE_H__
#define __SHUFFLE_H__

#define PID_FILE "/.cdshuffle_"  /* add number 0..9 */

void do_shuffle(
		cdtool_t *cdt,   /* cdtool handle */ 
		int iCDNum,      /* CDROM device 0..9 */
		int iTracks,     /* max number tracks to play */
		int iCount       /* times to repeat play this CD */
		);

int shuffle_lock(int cdnum);
int shuffle_unlock(int cdnum, int stop);

#endif
		
