#ifndef _UTIL_H
#define _UTIL_H

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include "config.h"
#include "cdtool.h"

/* version string printed with usage message */ 
#define VERSION_STRING "CDTOOL " PACKAGE_VERSION \
  " (c) 1994-2004 Thomas Insel, et al.  Licensed under GPL."

/* general definitions */
#ifndef TRUE
  #define TRUE (1==1)
#endif

#ifndef FALSE
  #define FALSE (1==0)
#endif

#define DOCRLF 2
#define DOLF   1
#define DONADA 0

#define BUGFOUND(x) bugfound(x, __FILE__, __FUNCTION__, __LINE__);
 
extern int cdtool_show_crlf;
extern int cdtool_show_verbose;
extern int cdtool_show_debug;

#ifndef HAVE_STRSEP
char *strsep(char **stringp, const char *delim);
#endif

void debugmsg(char *fmt, ...);
void errormsg(char *fmt, ...);
void verbosemsg(char *fmt, ...);
void infomsg(char *fmt, ...);
void bugfound(char *message, char *file, char *function, int line);
void show_permissions(char *device);
void do_crlf(FILE *term);

char *device_detect(char *choice);
int is_device(char *path, int verbose);
int checkmount(char *pszName);

char *getprogname(void);
char *setprogname(char *name);

#endif
