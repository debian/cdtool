/* hardware.h */

#ifndef __HARDWARE_H__
#define __HARDWARE_H__

#include "config.h"

#ifdef HAVE_IOCTL_H
# include <ioctl.h>
#elif defined HAVE_SYS_IOCTL_H
# include <sys/ioctl.h>
#else
# error found no ioctl.h
#endif

#ifdef HAVE_LINUX_CDROM_H
# include <linux/cdrom.h>
#elif defined HAVE_SYS_CDIO_H
# include <sys/cdio.h>
#else
# error found no cdrom.h or cdio.h
#endif

/* additional error codes, see linux/cdrom.h */
#define NO_CDROM         200
#define CDROM_MOUNTED    201
#define CDROM_DATA_DISC  202

/* return codes from cd_status, these are derived from XPlaycd code */
#define CD_STATUS_ERROR    -1
#define CD_STATUS_NO_CD     0
#define CD_STATUS_CD_IN     1
#define CD_STATUS_INSERTED  2
#define CD_STATUS_REMOVED   3
#define CD_STATUS_MOUNTED   4
#define CD_STATUS_DATA_DISC 5

#define MAX_TRACKS 99

/* state of the CD-ROM hardware */
typedef struct {
    int    iStatus;                        /* see CD_STATUS* below */
    int    iData;
    int    iAudio;
    struct cdrom_tochdr tochdr;            /* from CDROMREADTOCHDR */
    struct cdrom_tocentry tocentries[100]; /* from CDROMREADTOCENTRY */
    struct cdrom_subchnl subchnl;          /* from CDROMSUBCHNL */
} cdhw_t;

/* procedures */
cdhw_t *read_hw(int fd);
int update_hw(cdhw_t *hw, int fd);

int check_audio_range(cdhw_t *hw, int trk0, int trk1);
int first_track(const cdhw_t *hw);
int last_track(const cdhw_t *hw);
int is_audio_track(const cdhw_t *hw, int track);

int check_disc(cdhw_t *hw);
char *cd_status_text (int iStatus);
int do_play_ti(int fd, cdhw_t *hw, struct cdrom_ti *ti);

#endif
