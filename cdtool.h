/* cdtool.h */
#ifndef _CDTOOL_H
#define _CDTOOL_H

#include "hardware.h"

/* commands for skiping in playback mode */
#define SKIPBACK	-1
#define SKIPFORWARD	-2
#define SKIPSTART	-3

/* commands to the do_info command */
#define I_TRACK		0x01
#define I_AUDIOSTATUS	0x02
#define I_DISP_REL	0x03
#define I_DISP_ABS	0x04
#define I_DISP_ALL	0x05

/* commands for do_dir:
 *  do_dir (program_name, cd_file, p_format, usedb, cd_device); */
#define P_QUICK		0x01
#define P_LONG		0x02
#define P_TEMPL		0x03
#define P_RAW		0x04

typedef struct cdtool {
    int fd;
    char *device;
    cdhw_t *hw;
} cdtool_t;

#endif
