#ifndef __COMMANDS_H__
#define __COMMANDS_H__

#include "cdtool.h"
#include "hardware.h"

/************************************************************************/
/* 
 * Header:	commands.h
 * Purpose:     to define commandd functions
 * Language/Compiler/O/S:   GCC 2.7.2
 * Author:      Wade Hampton
 * Date:        12/23/97
 * Revision History:
 * [from RCS]
 *
 * $Log: commands.h,v $
 * Revision 1.1  1997/12/24 15:43:46  wadeh
 * Initial revision
 *
 *
 * Notes:
 * 1) 
 *----------------------------------------------------------------------*/
/* TYPES- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* INCLUDE FILES- - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* CONSTANTS- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* LOCAL DATA - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* EXTERNAL PROCEDURES- - - - - - - - - - - - - - - - - - - - - - - - - */
/* EXTERNAL DATA- - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* PROTOTYPES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

int cdtool_open(cdtool_t *cdtool, char *device);

int do_play(cdtool_t *cdt, int trk0, int trk1);
int do_skip(cdtool_t *cdt, int direction, int skip);
int do_pause(cdtool_t *cdt);
int do_stop(cdtool_t *cdt, int iCDNum);
int do_eject(cdtool_t *cdt);
int do_close(cdtool_t *cdt);
int do_reset(cdtool_t *cdt);
int do_volume(cdtool_t *cdt, int volume);

/*
 * Copyright (C) 1997, 1998 Wade Hampton
 */
/************************************************************************/
#endif


