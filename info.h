#ifndef __INFO_H__
#define __INFO_H__
/************************************************************************/
/* 
 * Header:	INFO
 * Purpose:     to define the interface to the INFO module
 * Language/Compiler/O/S:   GCC 2.7.2
 * Author:      Wade Hampton
 * Date:        12/22/97
 * Revision History:
 * [from RCS]
 *
 * $Log: info.h,v $
 * Revision 1.4  1998/07/15 13:53:36  wadeh
 * prior to Lin's changes
 *
 * Revision 1.3  1998/01/06 21:00:33  wadeh
 * Added DoCr, changes to fix cdeject crash.
 *
 * Revision 1.2  1997/12/23 22:52:51  wadeh
 * 2.1 beta 1 release
 *
 * Revision 1.1  1997/12/23 21:39:39  wadeh
 * Initial revision
 *
 *
 * Notes:
 * 1) 
 *----------------------------------------------------------------------*/
/* TYPES- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* INCLUDE FILES- - - - - - - - - - - - - - - - - - - - - - - - - - - - */
#include "cdtool.h"

/* CONSTANTS- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* commands to the do_info command */
#define I_TRACK		0x01
#define I_AUDIOSTATUS	0x02
#define I_DISP_REL	0x03
#define I_DISP_ABS	0x04
#define I_DISP_ALL	0x05

/* commands to the do_dir command */
#define P_QUICK		0x01
#define P_LONG		0x02
#define P_TEMPL		0x03
#define P_RAW		0x04

/* LOCAL DATA - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* EXTERNAL PROCEDURES- - - - - - - - - - - - - - - - - - - - - - - - - */
/* EXTERNAL DATA- - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* PROTOTYPES - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
/* DO_INFO:  Display info about CD-ROM to stdout */
void do_info(
             cdtool_t *cdtool, /* cdtool handle */ 
	     int info          /* INFO command, I_* above */
	     );

/* DO_DIR:  Display info on the CD-ROM (reads optional database) */
void do_dir  (
             cdtool_t *cdtool, /* name of progarm */ 
	     int p_format,     /* command, P_* above */
	     int usedb         /* T/F, if T, try to read the database (text)*/
	     );      
/*
 * Copyright (C) 1997, 1998 Wade Hampton
 */
/************************************************************************/
#endif


