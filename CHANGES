CHANGES 2.1.8 RELEASE  ----------------------------------------------------

 Max Vozeler <max@hinterhof.net>
   o Remove the outdated .spec file
   o Release otherwise unchanged 2.1.8pre4 as 2.1.8

CHANGES 2.1.8pre4 RELEASE -------------------------------------------------

 Max Vozeler <max@hinterhof.net>
   o Make commands.c ignore CDROM_AUDIO_INVALID status to make it work
     again with drives like the Toshiba SD-M1612. Thanks to Dani Marti 
     <daniel.marti@upf.edu> for the bug report.

CHANGES 2.1.8pre3 RELEASE -------------------------------------------------

 Arnaud Patard <arnaud.patard@rtp-net.org>
   o Fix the playing range

 Max Vozeler <max@hinterhof.net>
   o Re-add the missing newline in cdinfo
   o Update address of the FSF in GPL copyright notices

CHANGES 2.1.8pre2 RELEASE -------------------------------------------------

 Kevin Ryde <user42@zip.com.au>
   o Correct man page .IP typos

 Max Vozeler <max@hinterhof.net>
   o Don't try detection if the user choice fails
   o cdctrl: Use device from first argument
   o Included GPL notices in the source files
   o Relicensed cdown.c under the GPL with Byron's permission.

CHANGES 2.1.8pre1 RELEASE ------------------------------------------------

 Bruce M. Simpson
   o Patch to add support for Solaris 2.5

 Max Vozeler <max@hinterhof.net>
   o Removed some unused tests from configure.ac
   o Adapt Bruce's patch to autoconf, compile-tested on Solaris 2.9
 
CHANGES 2.1.7 RELEASE ----------------------------------------------------

 Peter Samuelson <peter@p12n.org>
   o Add cdtool2cddb to convert .cdtooldb to CDDB files
   o Add manpage cdtool2cddb.1

 Max Vozeler <max@hinterhof.net>
   o Fix the install target
   o Clarify copyrights in README
   o Update README and INSTALL
 
CHANGES 2.1.6 RELEASE ----------------------------------------------------

 Roland Rosenfeld <roland@debian.org>
   o Add manpage cdloop.1
 
 Andrea Fanfani <andrea@debian.org>
   o Fix potential /tmp exploit in cdshuffle

 Peter Samuelson <peter@p12n.org>
   o Patch reintroducing cdvolume.
   o Patch adding -r for raw CDDB output to cdown.
   o Disable broken /etc/mtab check. The error message is just as informative.
   o Patch to improve linefeed handling.
   o Fix uninitialized buffer in cdir database code.

 Josh Buhl <jbuhl@gmx.net>
   o Add cdclose command to close the CDROM tray. 
  
 Martin Mitchell <martin@debian.org>
   o Change all CD device opens to nonblocking.

 Mike Coleman <mkc@kc.net>
   o Patch to handle mixed audio and data CDs correctly.

 Gergely Nagy <algernon@debian.org>
   o Changed cdown.c and cdown.1 to default to freedb.freedb.org
   o Applied patch from #53347 to remove spurios whitespace from cdown
   o Appiled patch from #82977 to improve error reporting when cdtool is
     unable to access the CD-ROM drive

 Ingo Wilken <Ingo.Wilken@informatik.uni-oldenburg.de>
   o Patch to commands.c for Plextor SCSI drives.

 Jakob Bratkovic <jakob.bratkovic@volja.net>
   o Patch to fix playing the last track

 Richard Kettlewell <rjk@greenend.org.uk>
   o Improve error messages when the cdrom device can't be opened

 Max Vozeler <max@hinterhof.net>
   o New build system
   o Initial Autotoolizing
   o cdloop:
     o Uses $CDTOOLDEV now
   o cdtool/cdown/cdctrl/cdloop:
     o Add automatic detection of known CDROM devices
     o Add -D argument for runtime debugging
   o cdadd:
     o Ask before committing data to ~/.cdtooldb
     o Use favourite $EDITOR with sensible fallback
   o Fix crash in cdshuffle when no CD is inserted
   o Check for sane number (<= MAX_TRACKS) of tracks in the TOC header.
     Fixes strange SIGSEGV on Debian/Sparc.
   o Document 2.2.17/2.2.18 audio CD kernel bugs
   o Code fixes
     o Rename main.[ch] to cdtool.[ch]
     o Rename s/cdfile/cdfd/g in many places
     o Replace vasprintf() with vsnprintf()
     o Remove ifdef CDCTRL
     o Remove EXIT()
     o Remove global variables, introduce cdtool_t
     o Replace DoCr() with newline handling in INFOMSG()/ERRORMSG()/..
     o Add strerror to some error fprintf arguments
     o Fix gcc 3.3 compiler warnings
     o Fix some typos

CHANGES SINCE 2.1.5 BETA RELEASE -----------------------------------------

(1) CDTOOLDBPATH environment variable supported.
(2) Bug in resuming from pause fixed.
(3) Templates printed with -t option now are "filled out" if the
    current disc is listed in a database.
(4) Make file creates links for manual pages during installation.
(5) Code completely reorganized, getopt() used, etc.
(6) Support for SunOS?  If you have gcc, this should compile and run
    fine under SunOS 4.1.*, but I don't have the facilities to test it.
    If you get it working, please let me know.
(7) CDTOOLDEV environment variable overrides default path to device.

Changes (1) - (4) by suggestion of Fred Baumgarten.  Thanks!

CHANGES SINCE 2.1.4 RELEASE ----------------------------------------------
(1) Thomas Insel's address is tinsel@tinsel.org.  An alternate mirror
    is at http://www.tinsel.org/files.
(2) Some SCSI CD-ROMs do not properly start playing with cdplay.  The 
    disc detection code has been cleaned up (but still needs some work).
(3) "cdplay" changed to "cdstart" to remove conflict with the cdplay from
    the cdp package.  To restore the previous cdplay, comment out the 
    LINKS line in Makefile and uncomment out the other one....
(4) Made option to make an RPM (make rpm).
(5) Added some code documentation.  Removed some warnings 
    when building with "make debug".
(6) Added "-p command" to cdtool for debugging (e.g., "cdtool -p info"
    is the same as "cdinfo").
(7) Shuffle mode added (written by Dan Risacher).
(8) Bug running cdir without a database fixed.
(9) Bug causing database lookup to fail fixed
(10) Added -8 and -9, since my CD-R shows up as /dev/scd8.
(11) Makefile changes to be more friendly to SCSI users and to properly
     make cdadd (missing from make all, make scsi, make scsidebug).
(12) Added ability to shuffle play 1..100 tracks 1..N times with the 
     -T and -R options.  To play a single track at random, use:
	cdshuffle -R 1 -T 1
(13) cdctrl shuffle option only plays 1 track, at random (like -R 1 -T 1).
(14) cdown added (written by Byron Ellacott with mods by W. Hampton).  
     This queries the CDDB database on the Internet and results in output
     sutiable for inclusion in the user's CDROM database (for cdir,
     cdinfo, etc.).
(15) Many fixes to hardware.c to properly recognize the state of the 
     CDROM, including code from XPlaycd (code which was originally from 
     WorkMan or WorkBone).
(16) cdshuffle writes a PID file in /tmp/cdshuffle_N (N=CDROM #).  
     Sending a SIGHUP will cause cdshuffle to skip to the next song.
     Sending a SIGINT will cause cdshuffle to stop playing, remove
     its PID file, and exit.
(17) cdstop looks for a cdshuffle PID file for this CDROM and if 
     present sends the cdshuffle a SIGINT to tell it to exit and cleanup.
(18) Several functions were changed to have cddevice an argument, and
     to not use the global cd_device per recommendation of Lin.
(19) cdctrl sometimes would cause segmentation violations due to 
     cd_device not being global.  Hacked, but cd_device should be
     made local and passed where needed (or better yet, pass a pointer
     to a CD control struture with {device name, device #, file des,
     debug fd, verbose flag, CRLF flag, etc.}).  Thanks Lin for 
     reporting this.... 

Most changes 2.0 -> 2.1 were done by Wade Hampton.
2.1.4-2.1.5 changes #7-11 by Daniel Risacher (magnus@mit.edu).
Change 14 was by Byron Ellacott.  Addition of a PID file and additonal
cleanup by Lin Zhe Min.

CHANGES SINCE 2.1.2 RELEASE ----------------------------------------------
(1) Fixed cdctrl to do a fflush after each command.  May now be started
    via inetd for remote CD-ROM control.
(2) Fixed makefile so "all" is the default make rule.

CHANGES SINCE 2.1.1 RELEASE ----------------------------------------------
(1) Fixed cdctrl eject command causing a segmentation violation.
(2) Fixed makefile debug option to not strip symbols.
(3) Added command.h.
(4) Fixed "read_hw" to always return a buffer, more robust.

CHANGES SINCE 2.0 RELEASE ----------------------------------------------
(1) SCSI build option added.  This uses the CDROMPLAYMSF ioctl 
    instead of the sometimes, not-supported CDROMPLAYTRKIND ioctl call
    (change to commands.c:do_play()).
(2) Makefile includes "scsi", "debug", and "debugscsi" options.
(3) Added new program, cdreset.  This resets the CDROM drive.
(4) Additional documentation.
(5) Additional range checks, more robust.
(6) Modules hardware.c, info.c, and commands.c now designed for 
    multiple calls.
(7) New program, cdctrl added. May be used as a CDROM daemon.
(8) New features added (see man page).
(9) Manual page updated and manual page for cdctrl written.

All changes 1.0 -> 2.0 were done by Sven Oliver Moll

CHANGES SINCE 1.0 RELEASE ----------------------------------------------
(1) Bug in Play From-Track-To-Track fixed.
(2) cdinfo command added.
(3) Code again reorganized.
(4) Skip track option in cdplay added.
(5) Multiple cdrom device support added.

